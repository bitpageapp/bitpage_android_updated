package com.bitpage.com.mqttchat.ViewHolders;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bitpage.com.R;
import com.bitpage.com.mqttchat.Adapters.ReceivedChatsAdapter;
import com.bitpage.com.mqttchat.AppController;


public class ViewHolderChatlist extends RecyclerView.ViewHolder  {
    public TextView newMessageTime, newMessage, storeName, newMessageDate, newMessageCount;
    public ImageView storeImage, tick;
    public RelativeLayout rl,delete_layout,rL_alert,cancel_layout,frame;
    //public LinearLayout rL_alert;

    public ViewHolderChatlist(View view) {
        super(view);


        newMessageTime = (TextView) view.findViewById(R.id.newMessageTime);
        newMessage = (TextView) view.findViewById(R.id.newMessage);
        newMessageDate = (TextView) view.findViewById(R.id.newMessageDate);
        storeName = (TextView) view.findViewById(R.id.storeName);
        storeImage = (ImageView) view.findViewById(R.id.storeImage2);
        tick = (ImageView) view.findViewById(R.id.tick);
        rl = (RelativeLayout) view.findViewById(R.id.rl);
        delete_layout = (RelativeLayout) view.findViewById(R.id.delete_layout);
        cancel_layout = (RelativeLayout) view.findViewById(R.id.cancel_layout);
        rL_alert = (RelativeLayout) view.findViewById(R.id.rL_alert);
        frame = (RelativeLayout) view.findViewById(R.id.frame);




        newMessageCount = (TextView) view.findViewById(R.id.newMessageCount);


        Typeface tf = AppController.getInstance().getRobotoCondensedFont();


        newMessageCount.setTypeface(tf, Typeface.BOLD);
        newMessageDate.setTypeface(tf, Typeface.NORMAL);
        newMessageTime.setTypeface(tf, Typeface.NORMAL);
        newMessage.setTypeface(tf, Typeface.NORMAL);

    }


}
