package com.bitpage.com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
/*import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;*/
import com.bitpage.com.R;
import com.bitpage.com.pojo_class.home_explore_pojo.ExploreResponseDatas;
import com.bitpage.com.utility.CommonClass;
import com.bitpage.com.utility.ProductItemClickListener;

import java.util.ArrayList;
import java.util.List;


/**
 * <h>ExploreRvAdapter</h>
 * <p>
 * In class is called from ExploreFrag. In this recyclerview adapter class we used to inflate
 * single_row_images layout and shows the all post posted by logged-in user.
 * </p>
 *
 * @since 4/6/2017
 */
public class ExploreRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = ExploreRvAdapter.class.getSimpleName();
    private Activity mActivity;
    private ArrayList<ExploreResponseDatas> arrayListExploreDatas;
    private final ProductItemClickListener animalItemClickListener;

    public ExploreRvAdapter(Activity mActivity, ArrayList<ExploreResponseDatas> arrayListExploreDatas, ProductItemClickListener animalItemClickListener) {
        this.mActivity = mActivity;
        this.arrayListExploreDatas = arrayListExploreDatas;
        this.animalItemClickListener = animalItemClickListener;

        System.out.println(TAG + " " + "al size in adap=" + arrayListExploreDatas);

        WindowManager wm = (WindowManager) mActivity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_explore, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final ExploreResponseDatas exploreResponseDatas = arrayListExploreDatas.get(position);

        String productName = exploreResponseDatas.getProductName();
        String productPrice = exploreResponseDatas.getPrice();
        String currency = getCurrencySymbol(exploreResponseDatas.getCurrency());
        String pricetag = productPrice + " " + currency;
        String productCategory;
        String likesStatus = exploreResponseDatas.getLikeStatus();

        try {
            productCategory = exploreResponseDatas.getCategoryData().get(0).getCategory();
        } catch (NullPointerException e) {
            productCategory = exploreResponseDatas.getCategory();
        }

        Log.d(TAG, "productCategory " + productCategory + "productName " + productName);


        final MyViewHolder exploreHoler = (MyViewHolder) holder;

        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) exploreHoler.itemView.getLayoutParams();
        layoutParams.setFullSpan(false);

        // set product name
        if (productName != null && !productName.isEmpty())
            exploreHoler.tV_productName.setText(productName);

        // set product price
        if (productPrice != null && !productPrice.isEmpty())
            exploreHoler.tV_productPrice.setText(pricetag);

        //set likes status icon (heart_fill or heart_unfill)
        if(likesStatus.equals("1")) {
            exploreHoler.iV_heart_icon.setImageResource(R.drawable.heart_fill_icon);
        } else {
            exploreHoler.iV_heart_icon.setImageResource(R.drawable.heart_unfill_icon);
        }


        //set product currency icon
        switch (currency) {
            case "BTC":
                exploreHoler.iV_currencyIcon.setImageResource(R.drawable.ic_btc);
                break;
            case "BCH":
                exploreHoler.iV_currencyIcon.setImageResource(R.drawable.ic_bch);
                break;
            case "ETH":
                exploreHoler.iV_currencyIcon.setImageResource(R.drawable.ic_eth);
                break;
            case "LTC":
                exploreHoler.iV_currencyIcon.setImageResource(R.drawable.ic_ltc);
                break;
            case "USD":
                exploreHoler.iV_currencyIcon.setImageResource(R.drawable.ic_usd);
                break;
            case "CAD":
                exploreHoler.iV_currencyIcon.setImageResource(R.drawable.ic_cdn);
                break;

        }


        // set category name
        if (productCategory != null && !productCategory.isEmpty())
            exploreHoler.tV_productCategory.setText(productCategory);

        String postedImageUrl = exploreResponseDatas.getMainUrl();

        String containerHeight = exploreResponseDatas.getContainerHeight();
        String containerWidth = exploreResponseDatas.getContainerWidth();
        String isPromoted = exploreResponseDatas.getIsPromoted();

        int deviceHalfWidth = CommonClass.getDeviceWidth(mActivity) / 2;
        int setHeight = 0;

        if (containerWidth != null && !containerWidth.isEmpty())
            setHeight = (Integer.parseInt(containerHeight) * deviceHalfWidth) / (Integer.parseInt(containerWidth));
        exploreHoler.iV_explore_img.getLayoutParams().height = setHeight;

        System.out.println(TAG + " " + "containerHeight=" + containerHeight + " " + "set height=" + setHeight + " " + "device half height=" + " " + CommonClass.getDeviceWidth(mActivity) + " " + CommonClass.getDeviceWidth(mActivity) / 2);

        String imageUrl = postedImageUrl.replace("upload/", "upload/c_fit,h_500,q_40,w_500/");
        // product image
        try {
            Glide.with(mActivity)
                    .load(imageUrl)
                    .asBitmap()
                    .fitCenter()
                    .placeholder(R.color.image_bg_color)
                    .error(R.color.image_bg_color)
                    .into(exploreHoler.iV_explore_img);
        } catch (OutOfMemoryError | IllegalArgumentException | NullPointerException e) {
            e.printStackTrace();
        }

        ViewCompat.setTransitionName(exploreHoler.iV_explore_img, exploreResponseDatas.getProductName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animalItemClickListener.onItemClick(holder.getAdapterPosition(), exploreHoler.iV_explore_img);
            }
        });

        // show featured tag with product
        if (isPromoted != null && !isPromoted.isEmpty()) {
            if (!isPromoted.equals("0")) {
                exploreHoler.rL_featured.setVisibility(View.VISIBLE);
            } else exploreHoler.rL_featured.setVisibility(View.GONE);
        } else exploreHoler.rL_featured.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return arrayListExploreDatas.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView iV_explore_img, iV_currencyIcon;
        private RelativeLayout rL_featured;
        private TextView tV_productPrice, tV_productName, tV_productCategory;
        private ImageView iV_heart_icon;

        MyViewHolder(View itemView) {
            super(itemView);

            tV_productName = (TextView) itemView.findViewById(R.id.tV_productName);
            tV_productPrice = (TextView) itemView.findViewById(R.id.tV_productPrice);
            tV_productCategory = (TextView) itemView.findViewById(R.id.tV_productCategory);
            iV_explore_img = (ImageView) itemView.findViewById(R.id.iV_image);
            rL_featured = (RelativeLayout) itemView.findViewById(R.id.rL_featured);
            iV_currencyIcon = (ImageView) itemView.findViewById(R.id.iV_currencyIcon);
            iV_heart_icon = (ImageView) itemView.findViewById(R.id.iV_heart_icon);
            rL_featured.setVisibility(View.GONE);
        }
    }


    private String getCurrencySymbol(String currency) {
        if (currency != null && !currency.isEmpty()) {
            //..from locale..//
            /*Currency c  = Currency.getInstance(currency);
            currency=c.getSymbol();
            tV_currency.setText(currency);*/

            //..from array..//
            String[] arrayCurrency = mActivity.getResources().getStringArray(R.array.currency_picker);

            if (arrayCurrency.length > 0) {
                String[] getCurrencyArr;
                for (String setCurrency : arrayCurrency) {
                    getCurrencyArr = setCurrency.split(",");
                    String currency_code = getCurrencyArr[1];
                    String currency_symbol = getCurrencyArr[2];

                    if (currency.equals(currency_code)) {
                        System.out.println(TAG + " " + "currency symbol=" + currency_symbol + " " + "my currency=" + currency);
                        return currency_symbol;
                    }
                }
            }
        }
        return currency;
    }
}
