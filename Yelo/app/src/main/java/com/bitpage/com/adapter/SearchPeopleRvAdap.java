package com.bitpage.com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bitpage.com.event_bus.BusProvider;
import com.bitpage.com.main.activity.FacebookFriendsActivity;
import com.bitpage.com.main.activity.LandingActivity;
import com.bitpage.com.main.activity.PhoneContactsActivity;
import com.bitpage.com.main.activity.SearchProductActivity;
import com.bitpage.com.main.view_pager.search_product.PeoplesFrag;
import com.bitpage.com.pojo_class.ProfileFollowingCount;
import com.bitpage.com.utility.ApiUrl;
import com.bitpage.com.utility.SessionManager;
import com.bitpage.com.utility.VariableConstants;
import com.squareup.picasso.Picasso;
import com.bitpage.com.R;
import com.bitpage.com.pojo_class.search_people_pojo.SearchPeopleUsers;
import com.bitpage.com.utility.CircleTransform;
import com.bitpage.com.utility.ClickListener;
import com.bitpage.com.utility.CommonClass;

import java.util.ArrayList;

/**
 * <h>SearchPeopleRvAdap</h>
 * <p>
 *     This class is called from PeoplesFrag class. In this recyclerview adapter class we used to inflate
 *     single_row_search_product layout and shows all searched people.
 * </p>
 * @since 29-Jun-17
 */
public class SearchPeopleRvAdap extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final int TYPE_HEADER=0;
    private static final int TYPE_ITEM=1;
    private Activity mActivity;
    private ArrayList<SearchPeopleUsers> aL_users;
    private ClickListener clickListener;
    private SessionManager mSessionManager;
    private String followingCount;

    /**
     * <p>
     *     This is simple constructor to initailize list datas and context.
     * </p>
     * @param mActivity The current context
     * @param aL_users The list datas
     */
    public SearchPeopleRvAdap(Activity mActivity, ArrayList<SearchPeopleUsers> aL_users, String followingCount) {
        this.mActivity = mActivity;
        this.aL_users = aL_users;
        this.followingCount = followingCount;
    }

    /**
     * <h>OnCreateViewHolder</h>
     * <p>
     *     In this method The adapter prepares the layout of the items by inflating the correct
     *     layout for the individual data elements.
     * </p>
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // View view= LayoutInflater.from(mActivity).inflate(R.layout.single_row_search_product,parent,false);
        //return new MyViewHolder(view);
        View view;
        switch (viewType)
        {
            case TYPE_HEADER :
                view= LayoutInflater.from(mActivity).inflate(R.layout.header_discover_people,parent,false);
                return new HeaderViewHolder(view);

            case TYPE_ITEM :
                view= LayoutInflater.from(mActivity).inflate(R.layout.single_row_search_people,parent,false);
                return new ItemViewHolder(view);
        }
        return null;
    }

    /**
     * <h>OnBindViewHolder</h>
     * <p>
     *     In this method Every visible entry in a recycler view is filled with the
     *     correct data model item by the adapter. Once a data item becomes visible,
     *     the adapter assigns this data to the individual widgets which he inflated
     *     earlier.
     * </p>
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position)
    {
        //header view
        if(holder instanceof HeaderViewHolder) {

            HeaderViewHolder headerViewHolder = (HeaderViewHolder)holder;


            //search facebook friends
            headerViewHolder.rL_connectTofb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, FacebookFriendsActivity.class);
                    mActivity.startActivityForResult(intent, VariableConstants.FB_FRIEND_REQ_CODE);
                }
            });

            //connect to contacts
            headerViewHolder.rL_connectToContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, PhoneContactsActivity.class);
                    mActivity.startActivityForResult(intent, VariableConstants.CONTACT_FRIEND_REQ_CODE);
                }
            });
        }




            // item view
            if (holder instanceof ItemViewHolder) {

                position = position - 1;
                String username, userImage, fullName;
                username = aL_users.get(position).getUsername();
                userImage = aL_users.get(position).getProfilePicUrl();
                fullName = aL_users.get(position).getFullName();
                String mFollowFlag = aL_users.get(position).getFollowStatus();

                Log.d("exe", "mFollowFlag= " + mFollowFlag);

                final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                // set Profile pic
                if (userImage != null && !userImage.isEmpty())
                    Picasso.with(mActivity)
                            .load(userImage)
                            .placeholder(R.drawable.default_circle_img)
                            .error(R.drawable.default_circle_img)
                            .transform(new CircleTransform())
                            .into(itemViewHolder.image);

                // set full name
                if (fullName != null && !fullName.isEmpty())
                    itemViewHolder.tV_heading.setText(fullName);

                // set user name
                if (username != null && !username.isEmpty())
                    itemViewHolder.tV_subHeading.setText(username);

                //set follow or following
                if(CommonClass.isNetworkAvailable(mActivity)) {
                    mSessionManager = new SessionManager(mActivity);
                    if(mSessionManager.getIsUserLoggedIn()) {

                        if (mFollowFlag.equals("1")) {
                            itemViewHolder.tV_follow.setText(mActivity.getResources().getString(R.string.followings));
                            itemViewHolder.tV_follow.setTextColor(mActivity.getResources().getColor(R.color.white));
                            itemViewHolder.relative_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape);
                        }

                    }
                }

                //follow or unfollow
                final String userName = username;
                itemViewHolder.relative_follow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url;
                        String mFollowFlag = aL_users.get(holder.getAdapterPosition()-1).getFollowStatus();

                        if(CommonClass.isNetworkAvailable(mActivity)) {
                            //follow
                            mSessionManager = new SessionManager(mActivity);
                            if (mSessionManager.getIsUserLoggedIn()) {

                                if(mFollowFlag != null && mFollowFlag.equals("0")) {
                                    url = ApiUrl.FOLLOW + userName;
                                    ((SearchProductActivity) mActivity).apiCall.followUserApi(url);
                                    itemViewHolder.tV_follow.setText(mActivity.getResources().getString(R.string.followings));
                                    itemViewHolder.tV_follow.setTextColor(mActivity.getResources().getColor(R.color.white));
                                    itemViewHolder.relative_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape);

                                    ProfileFollowingCount follingCountObj = new ProfileFollowingCount();
                                    //follingCountObj.setFollowingCount(uniqueSet.size());
                                    BusProvider.getInstance().post(follingCountObj);
                                }
                                else {
                                    url = ApiUrl.UNFOLLOW + userName;
                                    ((SearchProductActivity)mActivity).apiCall.followUserApi(url);
                                    itemViewHolder.tV_follow.setText(mActivity.getResources().getString(R.string.follow));
                                    itemViewHolder.tV_follow.setTextColor(mActivity.getResources().getColor(R.color.colorPrimary));
                                    itemViewHolder.relative_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape);
                                }

                            } else {
                                Intent intent = new Intent(mActivity, LandingActivity.class);
                                mActivity.startActivityForResult(intent, VariableConstants.LANDING_REQ_CODE);
                            }
                        } else {
                            //CommonClass.showSnackbarMessage(, mActivity.getResources().getString(R.string.NoInternetAccess));
                        }
                    }
                });
            }

    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    @Override
    public int getItemCount() {
        return aL_users.size()+1;
    }


    /**
     * <h>GetItemViewType</h>
     * <p>
     *     In this method we used to return the type whether it is Header or item.
     * </p>
     * @param position The position of the row.
     * @return it returns the type of Item
     */
    @Override
    public int getItemViewType(int position) {
        if (position==0)
            return TYPE_HEADER;
        else return TYPE_ITEM;
    }


    /**
     * Recycler View item variables
     */
    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView tV_heading,tV_subHeading;
        RelativeLayout relative_follow;
        LinearLayout linear_rootElement;


        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener!=null)
                        clickListener.onItemClick(v,getAdapterPosition());
                }
            });
            image= (ImageView) itemView.findViewById(R.id.image);
            image.getLayoutParams().width= CommonClass.getDeviceWidth(mActivity)/7;
            image.getLayoutParams().height= CommonClass.getDeviceWidth(mActivity)/7;
            tV_heading= (TextView) itemView.findViewById(R.id.tV_heading);
            tV_subHeading= (TextView) itemView.findViewById(R.id.tV_subHeading);
            relative_follow = (RelativeLayout) itemView.findViewById(R.id.relative_follow);
            linear_rootElement = (LinearLayout) itemView.findViewById(R.id.linear_rootElement);
        }
    }

    /**
     * In this class we used to declare and assign the HeaderView variables.
     */
    private class HeaderViewHolder extends RecyclerView.ViewHolder
    {
        RelativeLayout rL_connectToContact,rL_connectTofb;

        HeaderViewHolder(View itemView) {
            super(itemView);
            rL_connectToContact= (RelativeLayout) itemView.findViewById(R.id.rL_connectToContact);
            rL_connectTofb= (RelativeLayout) itemView.findViewById(R.id.rL_connectTofb);
            //tV_fb_friends_count= (TextView) itemView.findViewById(R.id.tV_fb_friends_count);
            //tV_contact_friend_count= (TextView) itemView.findViewById(R.id.tV_contect_friend_count);
        }
    }


    /**
     * In this class we used to declare and assign the Item variables.
     */
    private class ItemViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView iV_profilePicUrl;
        private TextView tV_postedByUserName,tV_postedByUserFullName,tV_hide,tV_follow,tV_noPost;
        private LinearLayout linear_postData;
        private RelativeLayout relative_follow,rL_follow,rL_memeberName;
        private HorizontalScrollView horizontal_posts;
        private View view_divider;
        ImageView image;
        TextView tV_heading,tV_subHeading;



        ItemViewHolder(View itemView) {
            super(itemView);
           /* iV_profilePicUrl= (ImageView) itemView.findViewById(R.id.iV_profilePicUrl);
            iV_profilePicUrl.getLayoutParams().width= CommonClass.getDeviceWidth(mActivity)/7;
            iV_profilePicUrl.getLayoutParams().height= CommonClass.getDeviceWidth(mActivity)/7;
            rL_follow= (RelativeLayout)itemView.findViewById(R.id.relative_follow);
            rL_memeberName= (RelativeLayout)itemView.findViewById(R.id.rL_memeberName);
            tV_postedByUserName= (TextView) itemView.findViewById(R.id.tV_postedByUserName);
            tV_postedByUserFullName= (TextView) itemView.findViewById(R.id.tV_postedByUserFullName);
            tV_hide= (TextView) itemView.findViewById(R.id.tV_hide);

            linear_postData= (LinearLayout) itemView.findViewById(R.id.linear_postData);
            horizontal_posts= (HorizontalScrollView) itemView.findViewById(R.id.horizontal_posts);
            view_divider=itemView.findViewById(R.id.view_divider);
            tV_noPost= (TextView) itemView.findViewById(R.id.tV_noPost);*/
            tV_follow= (TextView) itemView.findViewById(R.id.tV_follow);
            relative_follow = (RelativeLayout) itemView.findViewById(R.id.relative_follow);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener!=null)
                        clickListener.onItemClick(v,getAdapterPosition()-1);
                }
            });
            image= (ImageView) itemView.findViewById(R.id.image);
            //image.getLayoutParams().width= CommonClass.getDeviceWidth(mActivity)/7;
            //image.getLayoutParams().height= CommonClass.getDeviceWidth(mActivity)/7;
            tV_heading= (TextView) itemView.findViewById(R.id.tV_heading);
            tV_subHeading= (TextView) itemView.findViewById(R.id.tV_subHeading);
        }
    }
    public void setOnItemClick(ClickListener listener)
    {
        clickListener=listener;
    }
}
